import _path from 'path'
import { NswagOptions } from './type'
import readline from 'readline'
import { Swagger } from './swagger'
import https from 'https'
import axios from 'axios'
/**
 * 生成代码
 * @param basePath 是否调试模式
 */
export default async (basePath: string) => {
  const configPath = _path.join(basePath, 'nswag/config.js')
  const nswagOptions = require(configPath) as NswagOptions

  nswagOptions.Apis.forEach((apiConfig) => {
    renderProgress(`正在生成 ${apiConfig.ApiName}`)
    getSwaggerData(apiConfig.SwaggerUrl).then((r) => {
      const swagger = new Swagger(basePath, apiConfig, r, nswagOptions.prettier)
      swagger.generate()
    })
    renderProgress(`${apiConfig.ApiName} 生成成功`)
  })
}

/**
 * 生成进度
 * @param text 文字
 * @param step 进度
 * @param isOk 完成
 */
function renderProgress(text: string) {
  readline.cursorTo(process.stdout, 0, 1)
  readline.clearScreenDown(process.stdout)
  process.stdout.write(`${text}`)
}

/**
 * 获取Swagger的JSON数据
 * @param swaggerUrl
 */
function getSwaggerData(swaggerUrl: string): Promise<any> {
  const agent = new https.Agent({
    rejectUnauthorized: false
  })
  return new Promise((resolve, reject) => {
    axios.get(swaggerUrl, { httpsAgent: agent }).then((response) => {
      if (response.status == 200) {
        const d = response.data
        if (typeof d == 'string') {
          const obj = eval('(' + d + ')')
          resolve(obj)
        } else {
          resolve(d)
        }
      } else {
        reject(new Error('获取swagger数据失败'))
      }
    })
  })
}
