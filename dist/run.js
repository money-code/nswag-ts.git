"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var readline_1 = __importDefault(require("readline"));
var swagger_1 = require("./swagger");
var https_1 = __importDefault(require("https"));
var axios_1 = __importDefault(require("axios"));
/**
 * 生成代码
 * @param basePath 是否调试模式
 */
exports.default = (function (basePath) { return __awaiter(void 0, void 0, void 0, function () {
    var configPath, nswagOptions;
    return __generator(this, function (_a) {
        configPath = path_1.default.join(basePath, 'nswag/config.js');
        nswagOptions = require(configPath);
        nswagOptions.Apis.forEach(function (apiConfig) {
            renderProgress("\u6B63\u5728\u751F\u6210 ".concat(apiConfig.ApiName));
            getSwaggerData(apiConfig.SwaggerUrl).then(function (r) {
                var swagger = new swagger_1.Swagger(basePath, apiConfig, r, nswagOptions.prettier);
                swagger.generate();
            });
            renderProgress("".concat(apiConfig.ApiName, " \u751F\u6210\u6210\u529F"));
        });
        return [2 /*return*/];
    });
}); });
/**
 * 生成进度
 * @param text 文字
 * @param step 进度
 * @param isOk 完成
 */
function renderProgress(text) {
    readline_1.default.cursorTo(process.stdout, 0, 1);
    readline_1.default.clearScreenDown(process.stdout);
    process.stdout.write("".concat(text));
}
/**
 * 获取Swagger的JSON数据
 * @param swaggerUrl
 */
function getSwaggerData(swaggerUrl) {
    var agent = new https_1.default.Agent({
        rejectUnauthorized: false
    });
    return new Promise(function (resolve, reject) {
        axios_1.default.get(swaggerUrl, { httpsAgent: agent }).then(function (response) {
            if (response.status == 200) {
                var d = response.data;
                if (typeof d == 'string') {
                    var obj = eval('(' + d + ')');
                    resolve(obj);
                }
                else {
                    resolve(d);
                }
            }
            else {
                reject(new Error('获取swagger数据失败'));
            }
        });
    });
}
//# sourceMappingURL=run.js.map